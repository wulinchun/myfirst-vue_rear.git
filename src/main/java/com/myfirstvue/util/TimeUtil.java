package com.myfirstvue.util;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author: wu linchun
 * @time: 2021/8/2 21:30
 * @description:
 */

public class TimeUtil {

    public final static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat sdf_date_format = new SimpleDateFormat("yyyy-MM-dd");

    private static SimpleDateFormat sdf_date_ym = new SimpleDateFormat("yyyyMM");

    public static Time stringToTime(String time) {
//        Time t = new Time(Long.parseLong(time));
//        try {
//            String hours = time.substring(0, 2);
//            String minutes = time.substring(3);
//            t.hour = Integer.parseInt(hours);
//            t.minute = Integer.parseInt(minutes);
//        } catch (Exception e) // TODO is this nice enough?
//        {
//            return null;
//        }
//
//        return t;
        return null;
    }

    public static Date transferEndDate(String dateTime) {
        if (dateTime.contains("Z")) {
            dateTime = dateTime.replace("Z", " UTC");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
            SimpleDateFormat defaultFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date time = format.parse(dateTime);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(time);
                int i = calendar.get(Calendar.HOUR);
                if (i == 0) {
                    calendar.add(Calendar.HOUR, 23);
                    calendar.add(Calendar.MINUTE, 59);
                    calendar.add(Calendar.SECOND, 59);
                }
                return calendar.getTime();
            } catch (Exception e) {
                e.printStackTrace();
                //log.error("格式化日期出错，{}", e);
            }
        }
        if (dateTime.length() == 10) {
            Date date = fomatDate(dateTime);
            if (date != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.HOUR, 23);
                calendar.add(Calendar.MINUTE, 59);
                calendar.add(Calendar.SECOND, 59);
                return calendar.getTime();
            }
            return null;
        }
        Date date = fomatDateYmdHms(dateTime);
        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int i = calendar.get(Calendar.HOUR);
            if (i == 0) {
                calendar.add(Calendar.HOUR, 23);
                calendar.add(Calendar.MINUTE, 59);
                calendar.add(Calendar.SECOND, 59);
            }
            return calendar.getTime();
        }
        return null;
    }

    private static Date fomatDate(String date) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            //log.error("格式化日期出错，{}", e);
            return null;
        }
    }

    /**
     * 格式化日期
     *
     * @param date
     * @return
     */
    public static Date fomatDateYmdHms(String date) {
        try {
            return sdfTime.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            //log.error("格式化日期出错，{}", e);
            return null;
        }
    }

    public static Date fomatDateYm(String date) {
        try {
            return sdf_date_ym.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            // log.error("格式化日期出错，{}", e);
            return null;
        }
    }
}
