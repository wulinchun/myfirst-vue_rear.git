package com.myfirstvue.controller;

import com.myfirstvue.entity.LoginUserPO;
import com.myfirstvue.entity.LoginUserVO;
import com.myfirstvue.mapper.LoginMapper;
import com.myfirstvue.service.LoginService;
import com.myfirstvue.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Time;
import java.util.Objects;

/**
 * @author: wu linchun
 * @time: 2021/7/25 13:42
 * @description:
 */
@RestController
@RequestMapping("/login")
public class LoginController {
    @Autowired
    @Qualifier("loginService")
    private LoginService loginService;

    @Autowired
    private LoginMapper loginMapper;

    @PostMapping("/handlelogin")
    public boolean handlelogin(@RequestBody LoginUserPO loginUserPO) {
        if (Objects.isNull(loginUserPO)) {
            return false;
        }
        return loginService.login(loginUserPO);
    }

    @PostMapping("/addlogin")
    public Integer addlogin(@RequestBody LoginUserVO loginUserVO) {
        if (Objects.isNull(loginUserVO)) {
            return 0;
        }
        LoginUserPO loginUserPO = new LoginUserPO();
        loginUserPO.setUserName(loginUserVO.getUserName());
        loginUserPO.setPassword(loginUserVO.getPassword());
        loginUserPO.setTime(new Time(TimeUtil.transferEndDate(loginUserVO.getTime()).getTime()));
        return loginMapper.insert(loginUserPO);
    }
}
