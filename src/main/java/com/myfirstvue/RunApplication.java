package com.myfirstvue;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: wu linchun
 * @time: 2021/7/25 13:44
 * @description:
 */
@SpringBootApplication
@MapperScan("com.myfirstvue.mapper")  //扫描mapper包下
public class RunApplication {
    public static void main(String[] args) {
        SpringApplication.run(RunApplication.class, args);
    }
}
