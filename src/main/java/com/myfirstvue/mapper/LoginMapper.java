package com.myfirstvue.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.myfirstvue.entity.LoginUserPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * @description:
 * @author: wu linchun
 * @time: 2021/7/25 13:34
 */
@Component("loginMapper")
public interface LoginMapper extends BaseMapper<LoginUserPO> {
}
