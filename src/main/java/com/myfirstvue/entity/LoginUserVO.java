package com.myfirstvue.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Time;

/**
 * @author: wu linchun
 * @time: 2021/8/2 21:27
 * @description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginUserVO implements Serializable {
    private String userName;

    private String password;

    private String time;
}
