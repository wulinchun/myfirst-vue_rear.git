package com.myfirstvue.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Time;

/**
 * @author: wu linchun
 * @time: 2021/7/25 13:30
 * @description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_login")
public class LoginUserPO implements Serializable {

    @TableField("uname")
    private String userName;

    @TableField("pwd")
    private String password;

    @TableField("time")
    private Time time;


}
