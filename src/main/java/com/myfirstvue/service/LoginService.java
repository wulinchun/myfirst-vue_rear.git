package com.myfirstvue.service;

import com.myfirstvue.entity.LoginUserPO;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: wu linchun
 * @time: 2021/7/25 13:36
 */
@Component
public interface LoginService {

    /**
     * @author: wu linchun
     * @create: 2021/7/25 13:38
     * @desc: 登录
     **/
    boolean login(LoginUserPO loginUserPO);


}
