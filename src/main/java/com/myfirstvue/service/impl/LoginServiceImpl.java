package com.myfirstvue.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.myfirstvue.entity.LoginUserPO;
import com.myfirstvue.mapper.LoginMapper;
import com.myfirstvue.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author: wu linchun
 * @time: 2021/7/25 13:38
 * @description:
 */
@Service("loginService")
public class LoginServiceImpl implements LoginService {
    @Autowired
    @Qualifier("loginMapper")
    private LoginMapper loginMapper;

    @Override
    public boolean login(LoginUserPO loginUserPO) {
        LambdaQueryWrapper<LoginUserPO> queryWrapper = new LambdaQueryWrapper<>();
        if (Objects.nonNull(loginUserPO)) {
            queryWrapper.eq(LoginUserPO::getUserName, loginUserPO.getUserName());
            String pwd = loginMapper.selectOne(queryWrapper).getPassword();
            if (pwd.equals(loginUserPO.getPassword())) {
                return true;
            }
        }
        return false;
    }
}
