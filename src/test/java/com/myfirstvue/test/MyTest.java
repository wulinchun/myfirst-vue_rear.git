package com.myfirstvue.test;

import com.myfirstvue.entity.LoginUserPO;
import com.myfirstvue.service.LoginService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author: wu linchun
 * @time: 2021/7/25 13:46
 * @description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MyTest {
    @Autowired
    @Qualifier("loginService")
    private LoginService loginService;

    @Test
    public void test1() {
        LoginUserPO loginUserPO = new LoginUserPO("张三", "123456",null);
        System.out.println(loginService.login(loginUserPO));
    }
}
